FROM node:14.13.0 as builder
RUN npm install -g gatsby-cli
WORKDIR /app
COPY ./package.json .
RUN yarn install && yarn cache clean
COPY . .
RUN ["yarn", "run", "build"]

FROM nginx:latest
EXPOSE 80
COPY --from=builder /app/public /usr/share/nginx/html