module.exports = {
  siteMetadata: {
    siteTitle: `Pantavanij`,
    defaultTitle: `Pantavanij`,
    siteTitleShort: `gatsby-theme-docs`,
    siteDescription: `OP Setup Guide`,
    siteUrl: `https://www.pantavanij.com/en/`,
    siteAuthor: `EP Dev @Pantavanij`,
    siteImage: `/banner.jpg`,
    siteLanguage: `en`,
    themeColor: `#7159c1`,
    basePath: `/`,
    footer: `EP Dev @Pantavanij`,
  },
  plugins: [
    {
      resolve: `@rocketseat/gatsby-theme-docs`,
      options: {
        configPath: `src/config`,
        docsPath: `src/docs`,
        // githubUrl: `https://github.com/rocketseat/gatsby-themes`,
        baseDir: `examples/gatsby-theme-docs`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Project Setup Guide`,
        short_name: `Pantavanij`,
        start_url: `/`,
        background_color: `#ffffff`,
        display: `standalone`,
        icon: `static/logo.png`,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://www.pantavanij.com/en/`,
      },
    },
    `gatsby-plugin-offline`,

    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-remark-images`,
      options: {
        linkImagesToOriginal: true, // Important!
      },
    },
    {
      resolve: `gatsby-remark-images-medium-zoom`, // Important!
      options: {},
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/images/`,
      },
    },
  ],
};
