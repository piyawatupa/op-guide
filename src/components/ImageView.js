import React from "react";
import Loadable from "@loadable/component";
const ImageZoom = Loadable(() => import("./ImageZoom"));

export const ImageView = (props) => {
  return <ImageZoom src={props.src} alt={props.alt} />;
};

ImageView.defaultProps = {
  src: "",
  alt: "",
};
export default ImageView;
