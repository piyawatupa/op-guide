import React from "react";
import mediumZoom from "medium-zoom";

function ImageZoom({ src, alt }) {
  if (typeof window === "undefined") {
    global.window = {};
  }

  const background = "rgba(0, 0, 0, 0.70)";
  const zoomRef = React.useRef(mediumZoom().clone({ background }));
  function attachZoom(image) {
    zoomRef.current.attach(image);
  }

  return (
    <img src={src} alt={alt} ref={attachZoom} width="100%" height="auto" />
  );
}

export default ImageZoom;
