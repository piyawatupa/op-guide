import React from "react";
import { isEmptyOrNull } from "../helpers/"
function ExternalLink({ url, text }) {

    text = isEmptyOrNull(text) ? text : url;

    return (
        <p><a href={url} target="_blank" rel="noreferrer">{text}</a></p>
    );
    
}

export default ExternalLink;
