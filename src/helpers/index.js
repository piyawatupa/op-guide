export const isEmptyOrNull = (item) => {
    if (item === undefined) return false;
    else return item === null || item.toString() === "" ? false : true;
}
