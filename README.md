## ⚡️ Getting started

1. Clone this project.

    ```
    git clone git@bitbucket.org:tanasit-int/op-guide.git
    ```

2. Install dependencies.

    ```
    npm install
    ```

3. Are you ready for launch? 
    ```
    npm start
    ```

    Your site is now running at `http://localhost:8000`

---

Made with 💜 by Rocketseat :wave: [check our community!](https://discordapp.com/invite/gCRAFhc)
